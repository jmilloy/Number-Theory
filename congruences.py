from operator import mul
from fractions import gcd
        
# Linear Congruences (Chinese Remainder Theorem)

def simplecrt(a,n):
    ''' chinese remainder theorem '''
    I = range(len(n))
    N = reduce(mul,n)
    c = [N/n[i] for i in I]
    d = [modinverse(c[i], n[i]) for i in I]
    x = sum([a[i]*c[i]*d[i] for i in I])
    return x%N,N

def crt(a, n):
    ''' general crt '''
    
    if len(n)==1:
        return a[0] % n[0], n[0]
    else:
        d = gcd(n[0],n[1])
        if (a[0] - a[1]) % d == 0:
            #reduce the first two congruences to one, and recurse
            A, N = simplecrt([a[0],a[1]], [n[0]/d, n[1]])
            return crt([A] + a[2:], [N] + n[2:])
        else:
            #the first two congruences are incompatible
            return None


#Polynomial Congruences (lifting solutions with Hensel's Lemma)

def horner(a,x):
    ''' evaluates polynomial a[0] + a[1]*x + a[2]*x^2 ... at x '''
    
    if len(a)==1:
        return a[0]
    else:
        return a[0] + x*horner(a[1:],x)

def hensel(p,e,x,f):
	''' applies hensel's lemma '''
	
    #calculate p^e and p^e+1
    p1 = p**e
    p2 = p1*p

    #make the derivative f'(x)
    df = [i*f[i] for i in range(1,len(f))]
    
    #evaluate f and f' at x
    fx = horner(f,x)
    dfx = horner(df,x)
        
    #The conditions of Hensel's Lemma
    if dfx % p != 0:
        #one solution
        ''' construction:

            f(x) = p^e * q
            q + f'(x) * k = 0 mod p
            x2 = x + p^e * k mod p^e+1
        '''
        q = fx / p1
        k = ((-q)*modinverse(dfx,p)) % p
        lift = (x + p1 * k) % p2
        return [lift]
        
    elif fx % p2 == 0:
        # p solutions
        lifts = []
        for i in range(p):
            lifts.append((x + p1*i) % p2)
        return lifts
        
    else:
        #no solutions
        return None
