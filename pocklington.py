import sys
import math
import random
import operator

from math import sqrt
from fractions import gcd

from util import divisor, fastexp, gen_primes
random.seed()

#=================
# useful functions
#=================

def factor(N):
    ''' get a list of factors of N '''
    factors = []
    primes = gen_primes()
    while N!=1:
        q = primes.next()
        if N%q==0:
            while N%q==0:
                N /= q
            factors.append(q)
    return factors

def factor_partial(N):
    ''' get a partial list of factors of N '''
    factors = []
    n = sqrt(N)
    while N > n:
        d = divisor(N)
        N /= d
        factors.append(d)
    return list(set(factors))

#===============================
# Trial Division Primality Test
#===============================

def trial_division_test(N, verbose=False):
    d = divisor(N)
    if d == N:
        return True
    else:
        if verbose: print "smallest proper divisor:", d
        return False

#============================
# Pocklington Primality Test
# ===========================

def primality(N, time=10, verbose=False):
    if N==2: return (2,3)
    if N==3: return (3,2)
    if N%2 == 0: return False
    
    factors = factor_partial(N-1)
    if verbose: print "partial factorization of %d=N-1: %s" % (N-1, factors)
    
    a = None
    for t in xrange(time):
        if a: a = random.randint(3,N-2)
        else: a = 2
        
        if fastexp(a,N-1,N) != 1:
            if verbose: print "a^(N-1) mod N = %d != 1, where a=%d" % (fastexp(a,N-1,N), a)
            return False
        
        for q in factors:
            d = gcd(fastexp(a,(N-1)/q,N) - 1, N)
            if d==N:
                continue
            elif d!=1:
                if verbose: print "gcd(a^(N-1)/q - 1, N) = %d, where a=%d, q=%d" % (d,a,q)
                return False
                
        return reduce(operator.mul,factors), a
    
    if t==time:
        return False
    else:
        return None

#==========================
# MAIN
#==========================

def usage():
	print "Usage: ./pocklington.py <trial|pock> <n|filename>"
	sys.exit(-1)

if __name__ == '__main__':
	try:
		f = open(sys.argv[2])
		line = True
		while line:
			line = f.readline()
			try: prime = int(line)
			except: continue
			
			if sys.argv[1] == 'trial': r = trial_division_test(prime)
			elif sys.argv[1] == 'pock': r = primality(prime)
			else: usage()
				
			print prime,r
			if r is False:
			    break
			
		f.close()
	except:
		N = int(sys.argv[2])
		if sys.argv[1] == 'trial':
			r = trial_division_test(N, verbose=True)
			if r: print "prime"
			else: print "composite"
		elif sys.argv[1] == 'pock':
			r = primality(N, verbose=True)
			if r: print "prime; certificate:", r
			else: print "composite"
		else: usage()
