import random
from fractions import gcd

random.seed()

def pollard(N):
    ''' returns a pseudo random factor of N '''
    c = random.randint(3,N-1)
    f = lambda x: (x^2 % N + c) % N
    
    x,y,d = 2,2,1
    while d==1:
        x = f(x)
        y = f(f(y))
        d = gcd(abs(x-y),N)
    return d


def brent(N):
    ''' returns a pseudo random factor of N, faster '''
    c = random.randint(3,N-1)
    f = lambda x: (x^2 % N + c) % N
    
    y = random.randint(1,N-1)
    m = random.randint(1,N-1)
    g,r,q = 1,1,1
    
    while g==1:
        x=y
        for i in xrange(r): y = f(y)
        k=0
        while k<r and g==1:
            ys = y
            for i in xrange(min(m,r-k)):
                y = f(y)
                q = (q*abs(x-y)) % N
            g = gcd(q,N)
            k += m
        r *= 2
    
    if g==N:
        while 1:
            ys = f(ys)
            g = gcd(abs(x-ys),N)
            if g>1: break
    
    return g


#=============================
# Some functions that use rho
#=============================

# NOTE I never ended up using these, they are rather unoptimized

def factor_rho(N, rho=brent):
    ''' returns the factors of N '''
    factors = []
    while N>1000:
        if primality(N,10):
            factors.append(N)
            return list(set(factors))
        else:
            d = rho(N)
            while d==N: d = rho(N)
            N /= d
            factors += factor_rho(d)
    
    factors += factor(N)
    return list(set(factors))

def factor_partial_rho(N, rho=brent):
    ''' get a proper factor of N '''
    
    d = [rho(N) for i in range(1000)]
    d = filter(lambda x: x!=N, d)
    if d: R = max(d)
    else: R = 2
    
    d = gcd(R,N/R)
    while d != 1:
        R *= d
        d = gcd(R, N/R)
    F = N/R
    if F<R: R,F = F,R
    return F,R

