def divisor(n):
    ''' returns the least divisor of n '''
    if n%2 == 0: return 2
    for i in xrange(3,int(sqrt(n)+1),2):
        if n%i == 0: return i
    return n

def lcm(n):
    ''' least common multiple '''
    if len(n) == 1:
        #lcm(a) = a
        return n[0]
    else:
        #lcm(n1, n2, n3, ... , nk) = lcm(m, n3, ... , nk) where m = lcm(n1,n2)
        m = n[0]*n[1] / gcd(n[0],n[1])
        return lcm([m]+n[2:])

def fastexp(a, k, n):
    ''' a^k mod n '''
    if a%n is 0: return 0
    
    exponents = [i for (i,c) in enumerate(bin(k)[::-1]) if c is '1']
    r=1
    for e in xrange(0,max(exponents)+1):
        if e in exponents:
            r = r*a % n
        a = a**2 % n
    return r

def gen_primes():
    """ Generate an infinite sequence of prime numbers.
        source: http://stackoverflow.com/questions/567222/simple-prime-generator-in-python
    """
    D = {}
    q = 2
    while True:
        if q not in D:
            #q prime
            yield q
            D[q * q] = [q]
        else:
            #q composite
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]
        q += 1

def bezout(a,b):
    ''' computes a solution to ax + by = gcd(a,b) '''
    
    #do the euclidean divisions, saving the q's
    qs = []
    while 1:
        q = int(a/b)
        r = a%b
        if 0==r: break
        qs.append(q)
        a,b = b,r
    
    #go through the steps in reverse, rebuilding a solution
    qs.reverse()
    x,y = 0,1
    for q in qs:
        if q==0: x,y = y,x
        else: x,y = y, x-y*q
    
    return x,y

def modinverse(a,n):
    ''' a^-1 mod n '''
    if gcd(a,n)==1:
        x,y = bezout(a%n,n)
        return x%n
    else:
        return None
